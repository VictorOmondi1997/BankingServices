/*
 * Design and develop a program that can be used for banking services where an
 * Account is opened by assigning it account number,
 * Account owners name and id and openning balance, the minimum opening balance.
 * is Ksh. 1000/=.
 * Once an account has been openned, account holder can, check balances, deposit
 * and can withdraw the maximum account balance .

 * DESIGN A PROGRAM
 */

/**
 *
 * @author Victor Omondi
 */
import java.util.Scanner;

public class Customer {
    private int accNo,
                idNo;
    private String accName;
    private double openingBal,
                   currentBal,
                   depositAmt;
    public Scanner sc = new Scanner(System.in);
    
    public Customer(){
        accNo = 0;
        idNo =0;
        accName= "Account Name Not Set";
        openingBal=0.00;
    }
    public Customer(int temp_accNo,
                    int temp_idNo,
                    String temp_accName,
                    double temp_openingBal,
                    double temp_depositAmt,
                    double temp_currentBal){
        accNo=temp_accNo;
        idNo=temp_idNo;
        accName=temp_accName;
        openingBal=temp_openingBal;
        currentBal=temp_currentBal;
        depositAmt=temp_depositAmt;
    }
    public void print(){
       currentBal=getDeposit()+openingBal;
       System.out.println("\n\nThe Following are your details: \n");
       System.out.println("\n*******************************************\n");
       System.out.println("Name: "+accName);
       System.out.println("idNo: "+idNo);
       System.out.println("Account Number: "+accNo);
       System.out.println("Current Balance: "+currentBal);
    }
    public void setDeposit(double temp_depositAmt){
        depositAmt=temp_depositAmt;
    }
    public double getDeposit(){
        return depositAmt;
    }
    public double withdraw(double w){
        currentBal=currentBal-w;
        return currentBal;
    }
    public double checkBalance(){
        return currentBal;
    }
    public static void main(String[] args){
        
        Scanner sc = new Scanner(System.in);
        String name="";
        int idNo=0,
            accNo=12345678;
        double openingBal=1000.00,
               deposit=0,
               currentBal = openingBal+deposit;
        Customer customer = new Customer(accNo,idNo, name,openingBal,deposit,currentBal);
        System.out.println("WELCOME TO VICKSTAR BANK\nOPEN NEW ACCOUNT");
        System.out.println("Enter the following DetailsTo Sign Up");
        System.out.println("Name: ");
        name=sc.nextLine();
        System.out.println("National id Number: ");
        idNo=sc.nextInt();
        System.out.println("Enter Opening Balance: ");
        openingBal=sc.nextDouble();
        while(openingBal<1000){
            System.out.println("Openning Balances Cannot be less that 1000");
            openingBal=sc.nextDouble();
        }
        customer.setDeposit(openingBal);
        System.out.println("Choose An Option");
        System.out.println("1.Deposit \t2.Withdraw \n3.check Balance \t4.Print User Details");
        System.out.print("Enter Option Here: ");
        int option;
        option=sc.nextInt();
        switch(option){
            case 1:
                System.out.println("Enter the Amount to Deposit");
                deposit=sc.nextDouble();
                while(deposit<50){
                    System.out.println("Error Deposit Should be Above 50 Shillings");
                    deposit=sc.nextDouble();
                }
                System.out.println("Success You have Deposited: "+deposit);
                customer.setDeposit(deposit);
                break;
            case 2:
                System.out.println("Enter Amount To Withdraw");
                double withdraw;
                withdraw=sc.nextDouble();
                customer.withdraw(withdraw);
                while(withdraw>=currentBal){
                    System.out.println("Failed: Your Current Balance: "+currentBal);
                    withdraw=sc.nextDouble();
                }
                break;
            case 3:
                System.out.println("Your Current Balance is: "+customer.checkBalance());
                break;
            case 4:
                customer.print();
                break;
            default:
                System.out.println("Option Not Found");
                break;
        }
    }
}
